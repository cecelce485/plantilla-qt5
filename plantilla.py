# QT Designer

# puedes crear una copia de este archivo sin los comentarios para usarlo como plantilla al crear un nuevo proyecto

# instalacion: 
# Abre una consola de comando y tipeas 
#    pip3 install PyQt5
#    pip3 install PyQt5-tools
# se instala en esta carpeta:
#   C:\Program Files\Python38\Lib\site-packages\pyqt5_tools

# el QT Designer:
#  https://build-system.fman.io/static/public/files/Qt%20Designer%20Setup.exe

# para Visual Studio uso una extencion PYQT Integration v0.2.0 de Feng Zhou
# agrega al menu contextual para archivos .ui la opcion de abrirlo con QT Designer,
# hay que configurarlo con la ruta donde se instalo el QT5
# tambien agrega resaltado de sintaxis a los .ui, los compila para pasarlos a .py y algunas cosas mas

# Recuerda siempre guardar el archivo .ui despues de modificar algo en la interfaz con QT Designer
# Al crear una nueva ventana principal, se puede eliminar la barra de menu haciendole click derecho
# aunque si no se le agrega nada tampoco se muestra al ejecutarse.
# cada texto que se le agregue se le asigna un nombre unico y luego se le puede agregar acciones al ser clickeada por ej
# como si fuera un boton cualquiera

import sys
from PyQt5 import uic
from PyQt5.QtWidgets import * # si no usas ventanas de alerta QMessageBox no se necesita importar esta sub_clase

# se podria escribir 
#   from PyQt5 import *
# pero se estaria agregando muchisimo codigo en este archivo el cual no es necesario

# en otros tutoriales vi que agregaban las siguientes lineas, para crear las ventanas de otra forma 
#   from PyQt5.QtGui import *
#   from PyQt5.QtCore import *
# como lo muesto me funciona


# las clases son los moldes que usaremos para crear objetos, puedes modificar GUI por otro nombre
class GUI(QMainWindow): # en este caso recibe como parametro QMainWindow porque se trata de una ventana
                        # aunque al crear un objeto no es necesario escribir nada, 
                        #   solamente: obj = GUI() y ya tenemos un nuevo objeto ventana

# Clases y objetos:
# https://notebook.community/gsorianob/fiuba-python/Clase%2009%20-%20%20Introducci%C3%B3n%20POO
                       

    def __init__(self):  # el self va siempre para referenciar al objeto actual
        # esta seccion de inicializacion la necesitan todos los objetos, aunque en ocaciones no es necesario
        # se ejecuta una sola vez al ser creado
       
        super().__init__()  # https://pythones.net/funcion-super-en-python-bien-explicada-ejemplos-oop/

        # modificar el nombre del .ui por el que se use, debe estar en la misma carpeta,
        uic.loadUi("plantilla.ui", self)
        # si esta en otra, ingresar la ruta "archivos_ui//plantilla.ui"
        # en las direcciones siempre se escriben con // y no con \
        # ya que se una como caracter de control, por ej /n salto de linea

        # se pueden declarar variables para ser usadas por este objeto
        self.valores = []
        # siempre se las accede con self.
        # el propio objeto accede a sus variables de la forma self.valores[1] = 2
        # mientras que desde fuera se accede nombre_obj.valores[1] = 2

        # https://es.stackoverflow.com/questions/90357/para-qu%C3%A9-se-utiliza-self-en-poo-en-python
        

        # de aca en adelante se asocian los eventos a cada elemento de la interfaz grafica, los widget
        self.boton_apretar.clicked.connect(self.ejecutar_al_apretar) 
        # clicked es uno de los eventos que tiene, en este caso, al ser presionado el boton 
        
        # dentro del QT5 se pueden modificar todas sus propiedades pero por supuesto desde aca tambien
        
        # y se puede hacer en cualquier punto del programa como cuando se quiere modificar el texto en un label
        self.label.setText("boton sin apretar") 
         

    # luego de la inicializacion se empieza con los metodos, no hay mas que funciones

    # este es un metodo del objeto
    def ejecutar_al_apretar(self): 

        self.label.setText("boton ya apretado")
            # en otros widgets se accede al texto directamente poniendo .text="algo"

            # https://pythonbasics.org/pyqt-buttons/

        # asi son las ventanas de alerta, Caja de Mensajes
        QMessageBox.question(self, 'Alert', "Boton apretado", QMessageBox.Ok)        
                # tipo de ventana   titulo,  texto que muestra    botones

        # tambien hay otras ventanas con boton ok, cancel y retorna el que se apreto
        # https://pythonprogramminglanguage.com/pyqt5-message-box/

        # otras formas de hacerlo
        # https://linuxhint.com/use-pyqt-qmessagebox/
        # https://www.tutorialspoint.com/pyqt/pyqt_qmessagebox.htm
        
# revisa paginas con las descripciones detalladas de todo lo que pueden hacer cada widget
# https://doc.qt.io/qtforpython-5/PySide2/QtWidgets/QComboBox.html#PySide2.QtWidgets.PySide2.QtWidgets.QComboBox.removeItem
# https://linuxhint.com/use-pyqt-qtablewidget/
# https://www.pythonguis.com/tutorials/pyqt-layouts/

# o busca en el mismo google con el nombre del widget que muestra por defecto en el Designer


if __name__ == "__main__": # esta linea sirve para corroborar de que se ejecuta desde este script 
                           # y no es llamado desde otro
                                 # https://www.freecodecamp.org/espanol/news/python-if-name-main/

    app = QApplication(sys.argv) # esta linea y la ultima siempre van asi, 
                                # llamadas al sistema para crear y cerrar la ventana

    un_objeto_gui = GUI()  # se crea una instancia de la clase    
    un_objeto_gui.show()   

    otro_objeto_gui = GUI()  # se pueden crear tantos objetos como se quieran usando el mismo molde   
    otro_objeto_gui.show()  # luego ejecuta el metodo de ese objeto, 

                    # todos los objetos estan aislados entre si, cada uno es una copia usando un mismo molde
                    
    sys.exit(app.exec_())  
